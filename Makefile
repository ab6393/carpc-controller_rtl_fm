BIN := carpc-controller
CC := gcc
CPP := g++
LD := g++

CPP_FLAGS := -Wall -Wextra -Wno-unused -Wno-unused-parameter -Wno-unknown-pragmas -pedantic
C_FLAGS := -std=gnu99
CXX_FLAGS := -std=c++11
C_FILES := $(wildcard src/*.c)
CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(C_FILES:.c=.o))) $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o))) 

INC_DIRS := -Iinclude -I/usr/local/include -I/usr/include/libusb-1.0

COMPILERTL := -lrtl-sdr
LINKRTL := -lrtlsdr

LIB_DIRS := -L/usr/local/lib
LIBS := -lwiringPi -pthread -lrt -lasound 


LD_FLAGS := $(LIB_DIRS) $(LIBS)
CC_FLAGS := $(INC_DIRS)

$(BIN): $(OBJ_FILES)
	$(CPP) $(LD_FLAGS) $(LINKRTL) -o $@ $^

obj/%.o: src/%.c
	mkdir -p obj
	$(CC) $(C_FLAGS) $(CC_FLAGS) $(CPP_FLAGS) $(LD_FLAGS) $(COMPILERTL) -c -o $@ $<

obj/%.o: src/%.cpp
	$(CPP) $(CXX_FLAGS) $(CC_FLAGS) $(CPP_FLAGS) -c -o $@ $<

clean:
	rm -rf obj/* carpc-controller
