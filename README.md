# README #

A modified version of CarPC-Controller for use with other radio devices, namely RTL_FM

### What is this repository for? ###

This tool is a modification of the standard CarPC-Controller created by Andrei Istodorescu.   
This allows non-Si chips to interract with the CarPC controller, and thus the CarPC project.  
It does so by checking if any RTL devices are connected, and gives control to RTL if one is found.  
I suppose a better solution would be a config file, but idk where the current config is.

### How do I get set up? ###

This project requires the following libraries:  
WiringPi (lwiringpi)  
PThread (pthread)  
ALSA Sound (lasound)  
RT Library (lrt)  
rtl-sdr (librtlsdr)  

Make (I use GNU Make) is also required. This can be easily installed from apt-get.

of these, Raspbian OS includes Pthread and LibRT.  
Note: If you are installing on existing builds of CarPC, WiringPi is installed already.  

For WiringPi (if it isn't already installed. On my version of Raspbian, it wasn't) directions are [here](http://wiringpi.com/download-and-install/). Note that this requires git.  
For simplicity's sake: 
```sh
$ cd
$ git clone git://git.drogon.net/wiringPi
$ cd ~/wiringPi
$ ./build
```
For ALSA
```sh
$ sudo apt-get install libasound2-dev
```
RTL-SDR instructions can be found [here](https://osmocom.org/projects/rtl-sdr/wiki). Note that this requires git, libusb-1.0 dev tools (libusb-1.0-0-dev on Raspbian Jessie) and either cmake or autotools.  
Make sure to install the UDev Rules. 

you will need to make the project first. a makefile is included.  

```sh
$ cd CarPC-Controller_RTL_FM
$ make
```

**For CarPC (aka the people who want to use this)**
carpc\_controller appears in \usr\bin\. replace it in this directory to continue. i will likely post a bash script later to automate this, after bugs are squashed.  


### Contribution guidelines ###

If any issues arise, especially if you use the original Si4703chip, please post on the boards and/or email me.  
abaum912@comcast.net  
If you would like to contribute, email me. 

### Who do I talk to? ###

for any issues on this particular tool, email or post on the boards. Note that i am not directly involved with the
project; i simply needed to modify it to work with what i have, and am posting in case others do too.  
for more information, please check  
http://engineeringdiy.freeforums.org/  
www.engineering-diy.blogspot.com  