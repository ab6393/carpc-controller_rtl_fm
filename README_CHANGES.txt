Changes From Original version (so as to comply with the GPL License V3)

all files:
Any reference to "si4703.h" in all files except "si4703.cpp" has been removed. it has been replaced with "radio.h" as needed. additionally, radio.cpp now includes si4703.h

line endings have been converted to windows endings (CRLF). this is a byproduct of visual studio.

spacing has been altered for readibility/ personal taste. no effect on the code. 

si4703.h, si4703.cpp
si_seek has been modified to take a function pointer, a reference to the update rds radio function. this is to prevent linker issues due to the above change.

si4703.h, common.h
most typedefs and structs in si4703.h have been moved to common.h to prevent linker issues and deal with above changes.

radio.h, radio.cpp
additional functions have been added. these functions mirror the si4703 functions (si_seek becomes radio_seek, etc). new functionality has been added here to allow any non si4703 devices to work by sending and recieving messages sent on socket 5006.

all files except radio.h, radio.cpp, si4703.h, si4703.cpp
any old references to si4703 functions have been replaced with the radio functions noted above. 

radio.cpp 
now includes socket and netinet to allow sockets as defined above.