#ifndef _COMMON_H_
#define _COMMON_H_

//Modified By Andrew Baumher, 12/19/2017
//moved typedefs here from si4703.h to avoid linker issues.

#include "config.h"
//#include <rpc/types.h>
#ifdef __cplusplus
extern "C" {
#endif

#if defined __cplusplus 

#elif defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
#include <stdbool.h>
#else 
	typedef enum
	{
		false = 0,
		true
	}bool;
#endif

/*
 * Defines
 */
#define LEFT                (0)
#define RIGHT               (1)

#ifndef __int8_t_defined
# define __int8_t_defined
typedef signed char     int8_t;
typedef short int       int16_t;
typedef int         int32_t;
# if __WORDSIZE == 64
typedef long int        int64_t;
# else
__extension__
typedef long long int       int64_t;
# endif
#endif

/* Unsigned.  */
typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
#ifndef __uint32_t_defined
typedef unsigned int        uint32_t;
# define __uint32_t_defined
#endif
#if __WORDSIZE == 64
typedef unsigned long int   uint64_t;
#else
__extension__
typedef unsigned long long int  uint64_t;
#endif

#define _BM(bit) (1 << ((uint16_t)bit)) // convert bit number to bit mask

#define SI4703_ADDR     0x10
/*#define SI4703_RESET    7
#define SI4703_SDA      8
#define SI4703_SCL      9
#define SI4703_INT      6*/
#define SI4703_RESET    4
#define SI4703_SDA      2
#define SI4703_SCL      3
#define SI4703_INT      25

#define FSEL_INPT       0
#define FSEL_OUTP       1
#define FSEL_ALT0       4
#define FSEL_ALT1       5
#define FSEL_ALT2       6
#define FSEL_ALT3       7
#define FSEL_ALT4       3
#define FSEL_ALT5       2

//Define the register names
#define DEVICEID   0x00
#define CHIPID     0x01
#define POWERCFG   0x02
#define DSMUTE      _BM(15)
#define DMUTE       _BM(14)
#define MONO        _BM(13)
#define RDSM        _BM(11)
#define SKMODE      _BM(10)
#define SEEKUP      _BM(9)
#define SEEK        _BM(8)
#define PWR_DISABLE _BM(6)
#define PWR_ENABLE  _BM(0)
// seek direction
#define SEEK_DOWN 0
#define SEEK_UP   1

#define CHANNEL    0x03
#define TUNE  _BM(15)
#define CHAN  0x003FF

#define SYSCONF1   0x04
#define RDSIEN _BM(15)
#define STCIEN _BM(14)
#define RDS    _BM(12)
#define DE     _BM(11)
#define AGCD   _BM(10)
#define BLNDADJ 0x00C0
#define GPIO3   0x0030
#define GPIO2   0x000C
#define GPIO1   0x0003

#define SYSCONF2   0x05
#define SEEKTH   0xFF00
#define BAND     0x00C0
#define SPACING  0x0030
#define VOLUME   0x000F
#define BAND0    0x0000 // 87.5 - 107 MHz (Europe, USA)
#define BAND1    0x0040 // 76-108 MHz (Japan wide band)
#define BAND2    0x0080 // 76-90 MHz (Japan)
#define SPACE50  0x0020 //  50 kHz spacing
#define SPACE100 0x0010 // 100 kHz (Europe, Japan)
#define SPACE200 0x0000 // 200 kHz (USA, Australia)

#define SYSCONF3   0x06
#define SMUTER 0xC000
#define SMUTEA 0x3000
#define RDSPRF _BM(9)
#define VOLEXT _BM(8)
#define SKSNR  0x00F0
#define SKCNT  0x000F

#define TEST1 0x07
#define XOSCEN _BM(15)
#define AHIZEN _BM(14)

#define TEST2    0x08
#define BOOTCONF 0x09

#define STATUSRSSI 0x0A
#define RDSR   _BM(15)
#define STC    _BM(14)
#define SFBL   _BM(13)
#define AFCRL  _BM(12)
#define RDSS   _BM(11)
#define BLERA  0x0600
#define STEREO _BM(8)
#define RSSI   0x00FF

#define READCHAN   0x0B
#define BLERB  0xC000
#define BLERC  0x3000
#define BLERD  0x0C00
#define RCHAN  0x03FF

#define RDSA       0x0C
#define RDSB       0x0D
#define RDSC       0x0E
#define RDSD       0x0F

/*
 * Typedefs
 */
typedef enum systemModes_tag {
    gModeXBMC_c = 0x00,
    gModeRadio_c = 0x01
} systemModes_t;

typedef struct encoder_tag {
    char actionLeft[ACTION_MAX_SIZE];
    char actionRight[ACTION_MAX_SIZE];
    uint8_t gpioClk;
    uint8_t gpioDt;
    uint8_t dtNew, dtOld, clkNew, clkOld, valOld, valNew;
    uint8_t changed, direction;
    uint8_t skipTimesLeft;
    uint8_t leftSteps;
    uint8_t skipTimesRight;
    uint8_t rightSteps;
} encoder_t;

typedef struct button_tag {
    char action[ACTION_MAX_SIZE];
    char *pCommands[MAX_COMMANDS]; /* Pointers to commands groups */
    uint8_t iCmd; /* Current index of command group */
    uint8_t nbCmd; /* Number of command groups */
    uint8_t gpio;
    uint8_t clickTimes;
    uint8_t clickHold;
    uint8_t click;
} button_t;

typedef struct rdsData_tag
{
	char programServiceName[9]; /* Program service name segment */
	char alternativeFreq[2];
	char radioText[64 + 1];
}rdsData_t;

typedef struct rdsStatus_tag
{
	bool stationName;
	bool radiotext;
} rdsStatus_t;

#ifdef __cplusplus
}
#endif

#endif /* _COMMON_H_ */
