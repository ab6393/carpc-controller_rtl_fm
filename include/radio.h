#ifndef _RADIO_H_
#define _RADIO_H_

//Modified By Andrew Baumher, 12/19/2017

#include "common.h"
#include "settings.h"

void Radio_Init(settings_t *pSettings);
void Radio_UnInit();

void Radio_FrequencyUpdate(int freq, bool resetRds);
void Radio_RdsUpdate();

//
int toRadioVolume(uint8_t systemVolume);
void init_radio();
void radio_power(uint16_t mode, uint8_t rst, uint8_t band, uint8_t spacing);
void radio_set_volume(int volume);
void radio_set_channel(int chan);
void radio_tune(int freq);
int radio_get_freq();
void radio_mute(int mode);
int radio_get_volume();
int radio_get_rssi();
int radio_seek(int dir);
void radio_rds_update(rdsData_t *pRdsData, rdsStatus_t *pStatus);
void radio_scan();
int radio_stop_scan();

#endif /* _COMMANDS_H_ */
