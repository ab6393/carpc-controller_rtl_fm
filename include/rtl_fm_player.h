#ifndef RTL_FM_PLAYER
#define RTL_FM_PLAYER

#include <pthread.h>
#include <rtl-sdr.h>

#pragma region
//#if defined __cplus_plus 
//
//#elif defined __STDC_VERSION__ && __STDC_VERSION__ >= 199901L
//#include <stdbool.h>
//#else 
//typedef enum
//{
//	false = 0,
//	true
//}bool;
//#endif
#include <stdbool.h>
#include <alsa/asoundlib.h>
#include <alsa/pcm.h>
#include <pthread.h>
#include <stdint.h>
#pragma endregion Boolean 

//Define if the project is not standalone,
#define _Integrated_Into_Other

#pragma region
#define DEFAULT_SAMPLE_RATE		32000
#define DEFAULT_BUF_LENGTH		(1 * 16384)
#define MAXIMUM_OVERSAMPLE		16
#define MAXIMUM_BUF_LENGTH		(MAXIMUM_OVERSAMPLE * DEFAULT_BUF_LENGTH)
#define AUTO_GAIN			100
#define BUFFER_DUMP			4096
#define FREQUENCIES_LIMIT		1000

#define PI2_F           6.28318531f
#define PI_F            3.14159265f
#define PI_2_F          1.5707963f
#define PI_4_F          0.78539816f

#define SQUELCH_DEFAULT  20

#define DEEMPHASIS_NONE         0
#define DEEMPHASIS_FM_EU        0.000050
#define DEEMPHASIS_FM_USA       0.000075

#define MIN_FREQ_DEFAULT        87500000
#define MAX_FREQ_DEFAULT       108000000
#define MIN_FREQ_JP             76000000
#define MAX_NB_FREQ_JP          90000000
#define BANDWIDTH_EU              100000
#define BANDWIDTH_US              200000
#define BANDWIDTH_OTHER            50000

#define DEFAULT_SCAN_DELAY       2000000


#define PCM_DEVICE "default"
#define CHAN_MONO 1
#define CHAN_STEREO 2
#define PERIOD_SIZE 1024
#define MIN_DATA 512
#pragma endregion Defines

#pragma region
typedef struct
{
	char * buf;
	unsigned int head;
	unsigned int tail;
	unsigned int size;
	unsigned int bytesAvailable;
} fifo_t;

//This initializes the FIFO structure with the given buffer and size
void fifo_init(fifo_t * f, unsigned int size);

//This reads nbytes bytes from the FIFO
//The number of bytes read is returned
int fifo_read(fifo_t * f, void * buf, unsigned int nbytes);

//This writes up to nbytes bytes to the FIFO
//If the head runs in to the tail, not all bytes are written
//The number of bytes written is returned
int fifo_write(fifo_t * f, const void * buf, unsigned int nbytes);

//simple check to see if the fifo has the number of bytes available. 
//easier to store in here than somewhere else.
//f: the fifo object, nbytes: number of bytes to check against
//does not consume the buffer.
//returns true if buffer has that number of bytes, false otherwise.
bool fifo_can_read_bytes(fifo_t * f, unsigned int nbytes);

void fifo_cleanup(fifo_t * f);

#pragma endregion FIFO buffer

#pragma region

enum STATUS { NONE, CLEAR, STABLE, SEEKING, SCANNING, TUNING, VOLUMECHANGED };

struct dongle_state
{
	int      exit_flag;
	pthread_t thread;
	rtlsdr_dev_t *dev;
	int      dev_index;
	uint32_t freq;
	uint32_t rate;
	int      gain;
	uint16_t buf16[MAXIMUM_BUF_LENGTH];
	uint32_t buf_len;
	int      ppm_error;
	bool     offset_tuning;
	bool     direct_sampling;
	uint32_t muteBytesAmount;
	bool     muteAll;
	struct demod_state *demod_target;
};
void dongle_init(struct dongle_state *dg);

struct demod_state
{
	int      exit_flag;
	pthread_t thread;
	int16_t  lowpassed[MAXIMUM_BUF_LENGTH];
	int      lp_len;
	int16_t  lp_i_hist[10][6];
	int16_t  lp_q_hist[10][6];
	int16_t  result[MAXIMUM_BUF_LENGTH];
	int16_t  droop_i_hist[9];
	int16_t  droop_q_hist[9];
	int      result_len;
	int      rate_in;
	int      rate_out;
	int      rate_out2;
	int      now_r, now_j;
	int      pre_r, pre_j;
	int      prev_index;
	int      downsample;    /* min 1, max 256 */
	int      post_downsample;
	int      output_scale;
	int      squelch_level, conseq_squelch, squelch_hits;
	int      freq_stable;
	bool     terminate_on_squelch;
	bool     squelch_exit;
	int      downsample_passes;
	int      comp_fir_size;
	int      custom_atan;
	bool     deemph;
	int      deemph_a;
	int      now_lpr;
	int      prev_lpr_index;
	bool     dc_block;
	int      dc_avg;
	void(*mode_demod)(struct demod_state*);
	pthread_rwlock_t rw;
	pthread_cond_t ready;
	pthread_mutex_t ready_m;
	struct output_state *output_target;
};
void demod_init(struct demod_state *dm);
void demod_cleanup(struct demod_state *d);

struct output_state
{
	int      exit_flag;
	pthread_t thread;
	int16_t  result[MAXIMUM_BUF_LENGTH];
	int      result_len;

	unsigned int rate;
	int volume;
	pthread_rwlock_t rw;
	pthread_cond_t ready;
	pthread_mutex_t ready_m;
	struct player_state * player_target;

};
void output_init(struct output_state *o);
void output_cleanup(struct output_state *o);

struct controller_state
{
	int      exit_flag;
	pthread_t thread;
	uint32_t freqs[FREQUENCIES_LIMIT];
	int      freq_len;
	int      freq_now;
	int      edge;
	bool     wb_mode;

	enum STATUS status;

	pthread_cond_t interact;
	pthread_mutex_t interact_m;
};
void controller_init(struct controller_state *c);
void controller_cleanup(struct controller_state *c);

struct interact_state
{

	pthread_rwlock_t status_lock;
	pthread_cond_t  scan;
	pthread_mutex_t scan_m;

	unsigned int freq_min, freq_max, bandwidth;
	enum STATUS status;

	bool seekUp;
	unsigned int freq;
	bool muted;
	int volume;
	int scan_delay;
	time_t start, end;
};
void interact_init(struct interact_state *i);
void interact_setup(int min, int max, int step);
void interact_cleanup(struct interact_state * i);

struct player_state
{
	//threading
	pthread_t thread;
	pthread_rwlock_t rw; //read-write lock. technically the reads are on a buffer, so it's both a read and write, but w/e.
						 //audio info
	unsigned int rate;
	unsigned int channels;
	snd_pcm_uframes_t period_size;
	//pcm 
	snd_pcm_t *pcm_handle; //pcm object. used for playing audio
	long pcm_error; //status of last pcm call. 0 
	snd_async_handler_t * pcm_callback; //callback for async pcm playing
	bool callbackRunning;

	//misc
	bool setup; //has player setup ran? (and therefore fifo initialized)
	fifo_t * fifo; //I/O buffer for the audio device
};
void player_init(struct player_state * p);
void player_setup(struct player_state * p);
void player_cleanup(struct player_state * p);

#pragma endregion Structs

#pragma region 

//interactive functions are mutually exclusive: if one is running, the others 
//are not. there is a weird case with scan, as it is successive seeks. if another
//is running, we cancel scan and wait for it to complete first. 

/// <summary>
/// A tune to a direct frequency. will cancel any running seeks or scans.
/// </summary>
/// <param name="freq">the frequency in hertz to tune to.</param>
void interact_tune(int freq);
/// <summary>
/// seeks for the next available frequency that passes squelch. cancels any existing scans.
/// </summary>
/// <param name="up">true if seeking up, false if down</param>
void interact_seek(bool up);

void interact_seek_with_callback(bool up, void(*RadioFrequencyUpdate)(int, bool));

/// <summary>
/// A step up or down from the current frequency. Will cancel any running seeks or scans
/// </summary>
/// <param name="up">true if stepping up, false if down</param>
void interact_step(bool up);
/// <summary>
/// Gets the current frequency.
/// </summary>
/// <returns>the current frequency in hertz</returns>
int  interact_get_freq();

/// <summary>
/// Scan: seeks to a stable frequency, pauses for two seconds, then seeks again, repeating until a user cancels.
/// cancels any existing seeks.
/// </summary>
void interact_scan();

/// <summary>
/// clears any existing seeks or scans.
/// </summary>
void interact_clear();

/// <summary>
/// Increments the volume counter. does not clear seek or scan
/// un-mutes the radio if muted
/// </summary>
void interact_inc_volume();
/// <summary>
/// Decrements the volume counter. does not clear seek or scan
/// does not un-mute the radio if muted.
/// </summary>
void interact_dec_volume();
/// <summary>
/// Sets the volume directly to an amount. does not clear seek or scan.
/// </summary>
/// <param name="volume">the volume to set the radio to</param>
void interact_set_volume(int volume);
/// <summary>
/// returns the current radio volume.
/// </summary>
/// <returns>the current radio volume</returns>
int  interact_get_volume();

/// <summary>
/// places up to strlen characters or RDS data into str
/// </summary>
/// <param name="str">the string that receives the RDS data</param>
/// <param name="strlen">maximum number of characters that can be written</param>
/// <returns>the number of characters written to str</returns>
int interact_get_rds(char * str, int strlen);

/// <summary>
/// Mutes or un-mutes the radio, based on isMuted. does nothing if
/// told to mute and already muted, or un-mute when already un-muted 
/// </summary>
/// <param name="isMuted">true when radio should be muted, false otherwise</param>
void interact_set_mute(bool isMuted);
#pragma endregion interactive Functions

/// <summary>
/// Tells the program to exit. Does not wait for other interacts to finish
/// </summary>
void interact_exit();

//struct thread functions
#pragma region
static void *dongle_thread_fn(void *arg);
static void *demod_thread_fn(void *arg);
static void *output_thread_fn(void *arg);
static void * player_thread_fn(void * arg);
static void *controller_thread_fn(void *arg);
#pragma endregion Threading Functions

//callbacks
#pragma region 
//callback for the dongle thread. executed repeatedly with each chunk of data.
static void rtlsdr_callback(unsigned char *buf, uint32_t len, void *ctx);
static void pcm_callback_fn(snd_async_handler_t *pcm_callback);
#pragma endregion Callbacks

//helpers. These should be private but then they need to be in a specific order to prevent compilation issues. i'd prefer to avoid that.
#pragma region 

static void sighandler(int signum);

/* more cond dumbness */
#define safe_cond_signal(n, m) pthread_mutex_lock(m); pthread_cond_signal(n); pthread_mutex_unlock(m)
#define safe_cond_wait(n, m) pthread_mutex_lock(m); pthread_cond_wait(n, m); pthread_mutex_unlock(m)
#define safe_cond_broadcast(n, m) pthread_mutex_lock(m); pthread_cond_broadcast(n, m); pthread_mutex_unlock(m)

/* {length, coef, coef, coef}  and scaled by 2^15
for now, only length 9, optimal way to get +85% bandwidth */
#define CIC_TABLE_MAX 10

#if defined(_MSC_VER) && (_MSC_VER < 1800)
double log2(double n)
{
	return log(n) / log(2.0);
}
#endif

void rotate_90(unsigned char *buf, uint32_t len);

void low_pass(struct demod_state *d);

int low_pass_simple(int16_t *signal2, int len, int step);

void low_pass_real(struct demod_state *s);

void fifth_order(int16_t *data, int length, int16_t *hist);

void generic_fir(int16_t *data, int length, int *fir, int16_t *hist);

/* define our own complex math ops
because ARMv5 has no hardware float */
void multiply(int ar, int aj, int br, int bj, int *cr, int *cj);

int polar_discriminant(int ar, int aj, int br, int bj);

int fast_atan2(int y, int x);

int polar_disc_fast(int ar, int aj, int br, int bj);

int polar_disc_lut(int ar, int aj, int br, int bj);

void fm_demod(struct demod_state *fm);

void am_demod(struct demod_state *fm);

void usb_demod(struct demod_state *fm);

void lsb_demod(struct demod_state *fm);

void raw_demod(struct demod_state *fm);

void deemph_filter(struct demod_state *fm);

void dc_block_filter(struct demod_state *fm);

int mad(int16_t *samples, int len, int step);

int rms(int16_t *samples, int len, int step);

void arbitrary_upsample(int16_t *buf1, int16_t *buf2, int len1, int len2);

void arbitrary_downsample(int16_t *buf1, int16_t *buf2, int len1, int len2);

void arbitrary_resample(int16_t *buf1, int16_t *buf2, int len1, int len2);

void full_demod(struct demod_state *d);

#pragma endregion "Private" RTL parsing helpers 

#ifdef _Integrated_Into_Other
int rtl_fm_init(int argc, char ** argv);
int rtl_fm_exit(int r);
#endif // _Integrate_Into_Other

#endif // !RTL_FM_PLAYER