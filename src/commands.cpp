#include <pthread.h>
#include <mqueue.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "radio.h"
#include "common.h"
#include "config.h"
#include "xbmcclient_if.h"
#include "event-server.h"
#include "settings.h"

/*
 * Defines
 */


/*
 * Typedefs
 */

/*
 * Private Functions Declarations
 */
static void Commands_Execute(char *pStr);
static void *Commands_ProcessLoop(void *pParam);

/*
 * Local Variables
 */
static settings_t       *mpSettings = NULL;
static pthread_t        mCommandsProcessThread;
static mqd_t            mCommandsMessageQueueFd;
static struct mq_attr   mCommandsMessageQueueBuf;
static bool             mForceStop = false;


/*
 * Global Variables
 */
extern bool           gRdsEnabled;
extern bool           gFrequencyChanged;

/*
 * Public functions definitions
 */
void Commands_Init(settings_t *pSettings)
{
	//pthread_mutex_init(&lockX, NULL);
	printf("Starting Commands Module\n");

	mpSettings = pSettings;

	/* Create message queue */
	
	mCommandsMessageQueueBuf.mq_msgsize = 512;
	mCommandsMessageQueueBuf.mq_maxmsg = 16;
	mCommandsMessageQueueFd = mq_open("/QCarPC", O_CREAT | O_RDWR, 600, &mCommandsMessageQueueBuf);

	if (mCommandsMessageQueueFd < 0)
	{
		perror("Error");
	}
	else
	{
		printf("init ok %d\n", mCommandsMessageQueueFd);

		/* Create radio periodically update thread */
		pthread_create(&mCommandsProcessThread, NULL, Commands_ProcessLoop, NULL);
	}
}

void Commands_UnInit()
{
	mq_close(mCommandsMessageQueueFd);
}

void Commands_Signal(char *pStr)
{
	int res;
	//printf("sending: %s\n", pStr);

	res = mq_send(mCommandsMessageQueueFd, pStr, strlen(pStr) + 1, 1);
	if (res < 0)
	{
		printf("error fd %d\n", mCommandsMessageQueueFd);
	}
}


/*
 * Private functions definitions
 */
static void *Commands_ProcessLoop(void *pParam)
{
	unsigned int msgPrio;
	void *pBuff = calloc(512, 1);

	while (!mForceStop)
	{
		int ret;

		ret = mq_receive(mCommandsMessageQueueFd, (char*) pBuff,
			mCommandsMessageQueueBuf.mq_msgsize, &msgPrio);
		if (ret >= 0) /* got a message */
		{
			Commands_Execute((char*) pBuff);
		}
		else if (ret < 0)
		{
			printf("received: %d\n", ret);
		}
	}

	pthread_exit(NULL);
	free(pBuff);

	return NULL;
}

void Commands_ProcessRadioSetup(char *aComm[], uint32_t cComm)
{
	uint32_t iComm = 0;
	uint8_t gpio2 = 0;
	uint8_t rst = 0;
	uint8_t band = 0;
	uint8_t spacing = 0;
	uint16_t frequency = 0;

	for (iComm = 0; iComm < cComm; iComm++)
	{
		char *pComm = aComm[iComm];

		if (strstr(pComm, "GPIO2_"))
		{
			gpio2 = (uint8_t) atoi(pComm + strlen("GPIO2_"));
		}
		else if (strstr(pComm, "RST_"))
		{
			rst = (uint8_t) atoi(pComm + strlen("RST_"));
		}
		else if (strstr(pComm, "BAND_"))
		{
			band = (uint8_t) atoi(pComm + strlen("BAND_"));
		}
		else if (strstr(pComm, "SPACE_"))
		{
			spacing = (uint8_t) atoi(pComm + strlen("SPACE_"));
		}
		else if (strstr(pComm, "FREQ_"))
		{
			frequency = (uint16_t) (atof(pComm + strlen("FREQ_")) * 100.0f);
		}
	}

	printf("*RADIO: band:%d freq:%d gpio2:%d rst:%d spacing:%d\n", band, frequency, gpio2, rst, spacing);

	/*  */
	if ((gpio2 != 0) /*&& (gpio2 != mpSettings->radioGpio2)*/ &&
		(rst != 0) /*&& (rst != mpSettings->radioRst)*/ &&
		//(band != 0) /*&& (band != mpSettings->band)*/ &&
		(spacing != 0) /*&& (spacing != mpSettings->radioSpacing)*/)
	{
		mpSettings->band = band;
		mpSettings->radioFrequency = frequency;
		mpSettings->radioGpio2 = gpio2;
		mpSettings->radioRst = rst;
		mpSettings->radioSpacing = spacing;


		init_radio();
		radio_power(PWR_ENABLE, mpSettings->radioRst, mpSettings->band,
			mpSettings->radioSpacing);
		radio_mute(1);
		radio_set_volume(toRadioVolume(mpSettings->systemVolume));
		radio_tune(mpSettings->radioFrequency);
	}

	if (frequency != 0)
	{
		radio_tune(mpSettings->radioFrequency);
	}
}

void Commands_ProcessEncoderSetup
(
	char *aComm[],
	uint32_t cComm
)
{
	uint32_t iComm = 0;
	uint8_t id = 0xFF;

	for (iComm = 0; iComm < cComm; iComm++)
	{
		char *pComm = aComm[iComm];

		if (id == 0xFF)
		{
			if (strstr(pComm, "ID_"))
			{
				id = (uint8_t)(atoi(pComm + strlen("ID_")) - 1);
			}
			else
			{
				break;
			}
		}
		else
		{
			if (strstr(pComm, "A_"))
			{
				mpSettings->encoders[id].gpioClk = (uint8_t) atoi(pComm + strlen("A_"));
			}
			else if (strstr(pComm, "B_"))
			{
				mpSettings->encoders[id].gpioDt = (uint8_t) atoi(pComm + strlen("B_"));
			}
			else if (strstr(pComm, "MAPL_"))
			{
				memcpy(mpSettings->encoders[id].actionLeft,
					pComm + strlen("MAPL_"),
					strlen(pComm) - strlen("MAPL_") + 1);
			}
			else if (strstr(pComm, "MAPR_"))
			{
				memcpy(mpSettings->encoders[id].actionRight,
					pComm + strlen("MAPR_"),
					strlen(pComm) - strlen("MAPR_"));
			}
		}
	}

	if (id != 0xFF)
	{
		printf("*ENC: %d A:%d, B: %d, L:%s, R:%s\n",
			id + 1, mpSettings->encoders[id].gpioClk,
			mpSettings->encoders[id].gpioDt,
			mpSettings->encoders[id].actionLeft,
			mpSettings->encoders[id].actionRight);
	}
}

void Commands_ProcessButtonSetup
(
	char *aComm[],
	uint32_t cComm
)
{
	uint32_t iComm = 0;
	uint8_t id = 0xFF;

	for (iComm = 0; iComm < cComm; iComm++)
	{
		char *pComm = aComm[iComm];

		if (id == 0xFF)
		{
			if (strstr(pComm, "ID_"))
			{
				id = (uint8_t)(atoi(pComm + strlen("ID_")) - 1);
			}
			else
			{
				break;
			}
		}
		else
		{
			if (strstr(pComm, "GPIO_"))
			{
				mpSettings->buttons[id].gpio = (uint8_t) atoi(pComm + strlen("GPIO_"));
			}
			else if (strstr(pComm, "MAPBTN_"))
			{
				memcpy(mpSettings->buttons[id].action,
					pComm + strlen("MAPBUTTON_"),
					strlen(pComm) - strlen("MAPBUTTON_") + 1);
			}
		}
	}

	if (id != 0xFF)
	{
		printf("*BTN: %d GPIO:%d, C:%s\n",
			id + 1, mpSettings->buttons[id].gpio,
			mpSettings->buttons[id].action);
	}
}

void Commands_ProcessSystemSetup(char *aComm[],	uint32_t cComm)
{
	uint32_t iComm = 0;

	for (iComm = 0; iComm < cComm; iComm++)
	{
		char *pComm = aComm[iComm];

		if (strstr(pComm, "VOL_"))
		{
			mpSettings->systemVolume = (uint8_t) atoi(pComm + strlen("VOL_"));
		}
		else if (strstr(pComm, "SMODE_"))
		{
			mpSettings->systemMode = (uint8_t) atoi(pComm + strlen("SMODE_"));
		}
	}

	printf("*system mode: %d, system volume: %d\n", mpSettings->systemMode,
		mpSettings->systemVolume);
	radio_set_volume(toRadioVolume(mpSettings->systemVolume));

	if (mpSettings->systemMode == gModeXBMC_c)
	{
		radio_mute(1);
	}
	else
	{
		radio_mute(0);
	}
}

/*! \brief Parse a string and send the commands found via sockets.
 *
 *  \param[IN/OUT] pStr pointer to the command that should be executed
 */
static void Commands_Execute(char *pStr)
{
	uint32_t commLength = 0;

	if (pStr)
	{
		uint32_t idx;
		uint8_t iComm = 0;
		char *aCommands[20] = { 0 };

		//printf("%s\n", pStr);

		/* Setup input command in multiple strings */
		commLength = strlen(pStr);
		iComm = 1;
		for (idx = 0; idx < commLength; idx++)
		{
			if (pStr[idx] == ';')
			{
				pStr[idx] = 0;
			}
			if (pStr[idx] == ',')
			{
				pStr[idx] = 0;
				aCommands[iComm] = &pStr[idx + 1];
				iComm++;
			}
		}

		/* RADIO:GPIO2_25,RST_4,BAND_0,SPACING_100,FREQUENCY_101.1 */
		if (strstr(pStr, "RADIO:"))
		{
			aCommands[0] = pStr + strlen("RADIO:");
			Commands_ProcessRadioSetup(aCommands, iComm);
		}
		/*  */
		else if (strstr(pStr, "SYSTEM:"))
		{
			aCommands[0] = pStr + strlen("SYSTEM:");
			Commands_ProcessSystemSetup(aCommands, iComm);
		}
		/* ENC:ID_1,A_11,B_9,MAPL_<left>,MAPR_<right> */
		else if (strstr(pStr, "ENC:"))
		{
			aCommands[0] = pStr + strlen("ENC:");
			Commands_ProcessEncoderSetup(aCommands, iComm);
		}
		/* BTN:ID_1,GPIO_1,MAPBTN_<btn> */
		else if (strstr(pStr, "BTN:"))
		{
			aCommands[0] = pStr + strlen("BTN:");
			Commands_ProcessButtonSetup(aCommands, iComm);
		}
		else if (strstr(pStr, "radio_seek_up"))
		{
			radio_seek(SEEK_UP);
		}
		else if (strstr(pStr, "radio_seek_down"))
		{
			radio_seek(SEEK_DOWN);
		}
		else if (strstr((const char*) pStr, RADIO_CMD_TUNE))
		{
			int frequency;
			char *pPos;
			char *pPos2;

			pPos = (char*) pStr + strlen(RADIO_CMD_TUNE);

			pPos2 = strchr(pPos, '.');
			*pPos2 = *(pPos2 + 1);
			*(pPos2 + 1) = *(pPos2 + 2);
			*(pPos2 + 2) = 0;

			frequency = atoi((const char*) (pStr + strlen(RADIO_CMD_TUNE)));
//            printf("new freq: %d\n", frequency);
//            printf("frequency to tune: %s\n", (char*)pStr + strlen(RADIO_CMD_TUNE));

			radio_tune(frequency * 10);
		}
	}
}