#include <pthread.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>

#include "common.h"
#include "config.h"
#include "si4703.h"
#include "xbmcclient_if.h"
#include "event-server.h"
#include "radio.h"
#include <rtl-sdr.h>
extern "C" {
#include "rtl_fm_player.h"
}
/*
 * Local Variables
 */
static settings_t   *mpSettings = NULL;
static rdsData_t    mRdsData;
static uint8_t      mTmpPSName[3][9];
static bool useSi = true;

static int r = 0;
/*
 * Global Variables
 */

/*
 * Public functions definitions
 */
void Radio_Init(settings_t *pSettings)
{
	//pthread_mutex_init(&lockX, NULL);
	printf("Starting Radio Module\n");

	memset(&mRdsData.programServiceName, ' ',
		sizeof(mRdsData.programServiceName));
	memset(&mRdsData.radioText, ' ', sizeof(mRdsData.radioText));

	memset(mTmpPSName, ' ', 3 * 9);
	mTmpPSName[0][8] = 0;
	mTmpPSName[1][8] = 0;
	mTmpPSName[2][8] = 0;
}

void Radio_UnInit()
{
	if (!useSi) 
	{
		interact_exit();
		rtl_fm_exit(r);
	}
}

void Radio_FrequencyUpdate(int freq, bool resetRds)
{
	char *pPos;
	char replyMsg[20];
	char aXbmcAction[150];

	sprintf(replyMsg, "%d", freq);

	pPos = replyMsg + strlen(replyMsg) - 2;
	//*(pPos + 2) = *(pPos + 1); /* no need for the last 0 in frequency */
	*(pPos + 1) = *pPos;
	*pPos = '.';

	sprintf(aXbmcAction, "SetProperty(Radio.Frequency,%s,10000)", replyMsg);
	XBMC_ClientAction(aXbmcAction);

	if (resetRds)
	{
		memset(&mRdsData.programServiceName, ' ',
			sizeof(mRdsData.programServiceName));
		memset(&mRdsData.radioText, ' ', sizeof(mRdsData.radioText));
		mRdsData.programServiceName[sizeof(mRdsData.programServiceName) - 1] = 0;
		mRdsData.radioText[sizeof(mRdsData.radioText) - 1] = 0;

		/* RDS Radiotext */
		sprintf(aXbmcAction, "SetProperty(Radio.RadioText,\"%s\",10000)",
			mRdsData.radioText);
		XBMC_ClientAction(aXbmcAction);

		/* RDS Program name */
		sprintf(aXbmcAction, "SetProperty(Radio.StationName,\"%s\",10000)",
			mRdsData.programServiceName);
		XBMC_ClientAction(aXbmcAction);
	}
}

void Radio_RdsUpdate()
{
	if (mpSettings->systemMode == gModeRadio_c)
	{
		rdsStatus_t rdsStatus;
		char        aXbmcAction[150];

		si_rds_update(&mRdsData, &rdsStatus);
		if (rdsStatus.radiotext)
		{
			/* RDS Radiotext */
			sprintf(aXbmcAction, "SetProperty(Radio.RadioText,\"%s\",10000)",
				mRdsData.radioText);
			XBMC_ClientAction(aXbmcAction);
			printf("RT: [%s]\n", aXbmcAction);
		}

		if (rdsStatus.stationName)
		{
			/* RDS Program name */
			sprintf(aXbmcAction, "SetProperty(Radio.StationName,\"%s\",10000)",
				mRdsData.programServiceName);
			XBMC_ClientAction(aXbmcAction);
			printf("SN: [%s]\n", aXbmcAction);
		}
	}
}

/// <summary>
/// Converts System Volume into current radio system volume.
/// </summary>
/// <param name="systemVolume">volume int XMBC/Kodi units (0-100)</param>
/// <returns></returns>
int toRadioVolume(uint8_t systemVolume)
{
	if (useSi)
	{
		int radioVol = 0;
		radioVol = (int) systemVolume / (100.0f / 15.0f);
		return radioVol;
	}
	return systemVolume;
}

/// <summary>
/// Initalize the radios so they are ready for use
/// </summary>
void init_radio()
{
	//if an rtlsdr device is detected, use it.
	useSi = rtlsdr_get_device_count() <= 0;
	if (useSi)
	{
		si_init();
	}
	else
	{
		char * args[9] = { "rtl_fm_player", "-M", "wbfm", "-f", "87.5M", "-l", "20", "-t", "20" };
		r = rtl_fm_init(9, args); 
	}
}

/// <summary>
/// Send initial data to the radio module. via wiring pi if si chip, otherwise use integrated rtl
/// </summary>
/// <param name="mode">the mode of the chip. SI only</param>
/// <param name="rst">most likely the relative signal strength. SI only</param>
/// <param name="band">radio bandwidth, which differs by region. </param>
/// <param name="spacing">spacing between available stations. (.1Mhz in Europe, .2Mhz US)</param>
void radio_power(uint16_t mode, uint8_t rst, uint8_t band, uint8_t spacing)
{
	if (useSi)
	{
		si_power(mode, rst, band, spacing);
	}
	else
	{
		//erase the buffer
		int bandStart, bandEnd;
		switch (band)
		{
		//Wide Band JP
		case 1: bandStart = 76000000; bandEnd = 108000000;
		break;
		//JP
		case 2: bandStart = 76000000; bandEnd = 90000000;
		//US, Europe
		default: bandStart = 87500000; bandEnd = 108000000;
		break;
		}
		interact_setup(bandStart, bandEnd, spacing);
	}
}

void radio_set_volume(int volume)
{
	if (useSi)
	{
		si_set_volume(volume);
	}
	else
	{
		interact_set_volume(volume);
	}
}

void radio_set_channel(int chan)
{

	if (useSi)
	{
		si_set_channel(chan);
	}
	else 
	{
		
		interact_tune(chan);
	}
}

void radio_tune(int freq)
{
	if (useSi)
	{
		si_tune(freq);
	}
	else
	{
		interact_tune(freq);
	}
}

int radio_get_freq()
{
	if (useSi)
	{
		si_read_regs();
		return si_get_freq();
	}
	else
	{
		return interact_get_freq();
	}
}

void radio_mute(int mode)
{
	if (useSi)
	{
		si_mute(mode);
	}
	else
	{
		interact_set_mute(mode == 0);
	}
}

int radio_get_volume()
{
	if (useSi)
	{
		return si_get_volume();
	}
	else 
	{
		return interact_get_volume();
	}
}

int radio_get_rssi()
{
	if (useSi)
	{
		return si_get_rssi();
	}
	else 
	{
		//not supported
		return 0;
	}
}

int radio_seek(int dir)
{
	//i'm sure there's a better way but screw it, this works.
	void(*functionptr)(int, bool);
	functionptr = &Radio_FrequencyUpdate;
	if (useSi)
	{
		return si_seek(dir, functionptr);
	}
	else
	{
		interact_seek_with_callback(dir, functionptr);
		return interact_get_freq();
	}
}

void radio_rds_update(rdsData_t *pRdsData, rdsStatus_t *pStatus)
{
	if (useSi)
	{
		si_rds_update(pRdsData, pStatus);
	}
	else
	{
		//not currently supported. in the works.
	}
}

void radio_scan()
{
	if (useSi)
	{
		//not supported
	}
	else 
	{
		interact_scan();
	}
}

int radio_stop_scan() 
{
	if (useSi)
	{
		//not supported
		return 0;
	}
	else
	{
		interact_clear();
		return interact_get_freq();
	}
}